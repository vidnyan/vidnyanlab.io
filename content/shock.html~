<!DOCTYPE html>
<html lang="mr">
<head>
<!-- 2021-09-26 Sun 16:55 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>विज्ञान केंद्र</title>
<meta name="generator" content="Org mode">
<meta name="author" content="विज्ञानदूत">
<style type="text/css">
 <!--/*--><![CDATA[/*><!--*/
  .title  { text-align: center;
             margin-bottom: .2em; }
  .subtitle { text-align: center;
              font-size: medium;
              font-weight: bold;
              margin-top:0; }
  .todo   { font-family: monospace; color: red; }
  .done   { font-family: monospace; color: green; }
  .priority { font-family: monospace; color: orange; }
  .tag    { background-color: #eee; font-family: monospace;
            padding: 2px; font-size: 80%; font-weight: normal; }
  .timestamp { color: #bebebe; }
  .timestamp-kwd { color: #5f9ea0; }
  .org-right  { margin-left: auto; margin-right: 0px;  text-align: right; }
  .org-left   { margin-left: 0px;  margin-right: auto; text-align: left; }
  .org-center { margin-left: auto; margin-right: auto; text-align: center; }
  .underline { text-decoration: underline; }
  #postamble p, #preamble p { font-size: 90%; margin: .2em; }
  p.verse { margin-left: 3%; }
  pre {
    border: 1px solid #ccc;
    box-shadow: 3px 3px 3px #eee;
    padding: 8pt;
    font-family: monospace;
    overflow: auto;
    margin: 1.2em;
  }
  pre.src {
    position: relative;
    overflow: visible;
    padding-top: 1.2em;
  }
  pre.src:before {
    display: none;
    position: absolute;
    background-color: white;
    top: -10px;
    right: 10px;
    padding: 3px;
    border: 1px solid black;
  }
  pre.src:hover:before { display: inline;}
  /* Languages per Org manual */
  pre.src-asymptote:before { content: 'Asymptote'; }
  pre.src-awk:before { content: 'Awk'; }
  pre.src-C:before { content: 'C'; }
  /* pre.src-C++ doesn't work in CSS */
  pre.src-clojure:before { content: 'Clojure'; }
  pre.src-css:before { content: 'CSS'; }
  pre.src-D:before { content: 'D'; }
  pre.src-ditaa:before { content: 'ditaa'; }
  pre.src-dot:before { content: 'Graphviz'; }
  pre.src-calc:before { content: 'Emacs Calc'; }
  pre.src-emacs-lisp:before { content: 'Emacs Lisp'; }
  pre.src-fortran:before { content: 'Fortran'; }
  pre.src-gnuplot:before { content: 'gnuplot'; }
  pre.src-haskell:before { content: 'Haskell'; }
  pre.src-hledger:before { content: 'hledger'; }
  pre.src-java:before { content: 'Java'; }
  pre.src-js:before { content: 'Javascript'; }
  pre.src-latex:before { content: 'LaTeX'; }
  pre.src-ledger:before { content: 'Ledger'; }
  pre.src-lisp:before { content: 'Lisp'; }
  pre.src-lilypond:before { content: 'Lilypond'; }
  pre.src-lua:before { content: 'Lua'; }
  pre.src-matlab:before { content: 'MATLAB'; }
  pre.src-mscgen:before { content: 'Mscgen'; }
  pre.src-ocaml:before { content: 'Objective Caml'; }
  pre.src-octave:before { content: 'Octave'; }
  pre.src-org:before { content: 'Org mode'; }
  pre.src-oz:before { content: 'OZ'; }
  pre.src-plantuml:before { content: 'Plantuml'; }
  pre.src-processing:before { content: 'Processing.js'; }
  pre.src-python:before { content: 'Python'; }
  pre.src-R:before { content: 'R'; }
  pre.src-ruby:before { content: 'Ruby'; }
  pre.src-sass:before { content: 'Sass'; }
  pre.src-scheme:before { content: 'Scheme'; }
  pre.src-screen:before { content: 'Gnu Screen'; }
  pre.src-sed:before { content: 'Sed'; }
  pre.src-sh:before { content: 'shell'; }
  pre.src-sql:before { content: 'SQL'; }
  pre.src-sqlite:before { content: 'SQLite'; }
  /* additional languages in org.el's org-babel-load-languages alist */
  pre.src-forth:before { content: 'Forth'; }
  pre.src-io:before { content: 'IO'; }
  pre.src-J:before { content: 'J'; }
  pre.src-makefile:before { content: 'Makefile'; }
  pre.src-maxima:before { content: 'Maxima'; }
  pre.src-perl:before { content: 'Perl'; }
  pre.src-picolisp:before { content: 'Pico Lisp'; }
  pre.src-scala:before { content: 'Scala'; }
  pre.src-shell:before { content: 'Shell Script'; }
  pre.src-ebnf2ps:before { content: 'ebfn2ps'; }
  /* additional language identifiers per "defun org-babel-execute"
       in ob-*.el */
  pre.src-cpp:before  { content: 'C++'; }
  pre.src-abc:before  { content: 'ABC'; }
  pre.src-coq:before  { content: 'Coq'; }
  pre.src-groovy:before  { content: 'Groovy'; }
  /* additional language identifiers from org-babel-shell-names in
     ob-shell.el: ob-shell is the only babel language using a lambda to put
     the execution function name together. */
  pre.src-bash:before  { content: 'bash'; }
  pre.src-csh:before  { content: 'csh'; }
  pre.src-ash:before  { content: 'ash'; }
  pre.src-dash:before  { content: 'dash'; }
  pre.src-ksh:before  { content: 'ksh'; }
  pre.src-mksh:before  { content: 'mksh'; }
  pre.src-posh:before  { content: 'posh'; }
  /* Additional Emacs modes also supported by the LaTeX listings package */
  pre.src-ada:before { content: 'Ada'; }
  pre.src-asm:before { content: 'Assembler'; }
  pre.src-caml:before { content: 'Caml'; }
  pre.src-delphi:before { content: 'Delphi'; }
  pre.src-html:before { content: 'HTML'; }
  pre.src-idl:before { content: 'IDL'; }
  pre.src-mercury:before { content: 'Mercury'; }
  pre.src-metapost:before { content: 'MetaPost'; }
  pre.src-modula-2:before { content: 'Modula-2'; }
  pre.src-pascal:before { content: 'Pascal'; }
  pre.src-ps:before { content: 'PostScript'; }
  pre.src-prolog:before { content: 'Prolog'; }
  pre.src-simula:before { content: 'Simula'; }
  pre.src-tcl:before { content: 'tcl'; }
  pre.src-tex:before { content: 'TeX'; }
  pre.src-plain-tex:before { content: 'Plain TeX'; }
  pre.src-verilog:before { content: 'Verilog'; }
  pre.src-vhdl:before { content: 'VHDL'; }
  pre.src-xml:before { content: 'XML'; }
  pre.src-nxml:before { content: 'XML'; }
  /* add a generic configuration mode; LaTeX export needs an additional
     (add-to-list 'org-latex-listings-langs '(conf " ")) in .emacs */
  pre.src-conf:before { content: 'Configuration File'; }

  table { border-collapse:collapse; }
  caption.t-above { caption-side: top; }
  caption.t-bottom { caption-side: bottom; }
  td, th { vertical-align:top;  }
  th.org-right  { text-align: center;  }
  th.org-left   { text-align: center;   }
  th.org-center { text-align: center; }
  td.org-right  { text-align: right;  }
  td.org-left   { text-align: left;   }
  td.org-center { text-align: center; }
  dt { font-weight: bold; }
  .footpara { display: inline; }
  .footdef  { margin-bottom: 1em; }
  .figure { padding: 1em; }
  .figure p { text-align: center; }
  .inlinetask {
    padding: 10px;
    border: 2px solid gray;
    margin: 10px;
    background: #ffffcc;
  }
  #org-div-home-and-up
   { text-align: right; font-size: 70%; white-space: nowrap; }
  textarea { overflow-x: auto; }
  .linenr { font-size: smaller }
  .code-highlighted { background-color: #ffff00; }
  .org-info-js_info-navigation { border-style: none; }
  #org-info-js_console-label
    { font-size: 10px; font-weight: bold; white-space: nowrap; }
  .org-info-js_search-highlight
    { background-color: #ffff00; color: #000000; font-weight: bold; }
  .org-svg { width: 90%; }
  /*]]>*/-->
</style>
<link rel="stylesheet" type="text/css" href="babastyle.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2018 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">विज्ञान केंद्र</h1>
</header><hr>
<p>
<a href="../index.html"><b>मुख्यपान</b></a>&emsp;&emsp;&emsp;&emsp;
<a href="../freeprojects.html"><b>मुक्त प्रकल्प</b></a>&emsp;&emsp;&emsp;&emsp;
<a href="../articles.html"><b>लेखमाला</b></a>&emsp;&emsp;&emsp;&emsp;
<a href="../activities.html"><b>उपक्रम</b></a>&emsp;&emsp;&emsp;&emsp; 
<a href="../contact.html"><b>संपर्क</b></a>&emsp;&emsp;&emsp;&emsp; 
</p>
<hr>
<div id="outline-container-org8759e20" class="outline-2">
<h2 id="org8759e20">विजेचा धक्का</h2>
<div class="outline-text-2" id="text-org8759e20">
<p>
विजेचा धक्का (electric shock)हा अनुभव आपण कधीतरी घेतलेला असतोच. त्या बद्दल मनात भीती देखील असते. योग्य काळजी घेतली तर हा
धोका टाळता येतो हे सुद्धा आपल्याला माहिती असतं.  पण विजेचा धक्का का आणि कसा बसतो या बद्दल माहिती फारच कमी जणांना असते.
</p>
</div>

<div id="outline-container-org0063e7f" class="outline-3">
<h3 id="org0063e7f">धक्क्याचा त्रास का होतो ?</h3>
<div class="outline-text-3" id="text-org0063e7f">
<p>
कोणत्याही जोरदार धक्क्याचा त्रास होतोच.  मग तो पाठीत मारलेला गुद्दा असेल, एखादी वस्तू अंगावर पडल्याने आलेला अनुभव असेल किंवा विजेचा
धक्का असेल.  जेव्हा कोणत्याही प्रकारची ऊर्जा आपल्या शरिरात बाहेरून सोडली जाते तेव्हा आपल्याला धक्का बसतो. फलंदाजाच्या शरिरावर गोलंदाजाने
टाकलेला चेंडू आदळला, तर चेंडूची गतिज ऊर्जा फलंदाजाच्या शरिरात सोडली जाते आणि त्याला धक्का बसतो.  विजेचा धक्का देखील विद्युत ऊर्जा 
आपल्या शरिरात खर्च होते त्यामुळे बसतो. 
</p>

<figure>
<img src="../img/eshock.png" alt="electric_shock"> 

<figcaption><span class="figure-number">Figure 1: </span>विजेचा धक्का</figcaption>
</figure>

<p>
ही ऊर्जा किती आहे त्यावर विजेच्या धक्क्याची तीव्रता अवलंबून असते. शरिरात शिरलेली विद्युत ऊर्जा दोन गोष्टींवर अवलंबून असते. 
</p>
<ul class="org-ul">
<li><b>विद्युत विभवांतर (voltage difference)</b>:- ज्या ठिकाणी विजेचा शरिराला स्पर्श होतो तेथील व्होल्टेजमधील फरक. वरील आकृतीत जी व्यक्ती
दाखवली आहे तिच्या पायापाशी (जमिनीवर) शून्य व्होल्टेज तर डाव्या हातापाशी २३० व्होल्ट आहेत. या व्होल्टेज मधील फरक २३० आहे.</li>
<li><b>शरिराचा विद्युतरोध</b>:- जर शरीर काही कारणामुळे ओले असेल तर विद्युतरोध कमी असतो. शांत मनस्थितीतील व्यक्तीचा विद्युतरोध भीतीग्रस्त
व्यक्तीपेक्षा जास्त असतो असे आढळले आहे. माणसाच्या शरिराचा विद्युतरोध सुमारे १ दशलक्ष ओहम असतो. जितका शरिराचा विद्युतरोध जास्त, तितका
विजेच्या धक्क्याचा परिणाम कमी. व्यक्तीनुसार विद्युतरोध बदलतो असेही आढळून आले आहे.</li>
</ul>
</div>
<div id="outline-container-org17aa3c4" class="outline-4">
<h4 id="org17aa3c4">गणिती सूत्रे</h4>
<div class="outline-text-4" id="text-org17aa3c4">
<p>
पुढील गणिती सूत्रे वापरून शरिरात शिरलेल्या विद्युत ऊर्जेचा तपशील काढता येतो.
</p>
<ul class="org-ul">
<li>विद्युत शक्ती = व्होल्टेजचा वर्ग / विद्युतरोध = व्होल्टेज x विद्युत प्रवाह<br>
<img src="ltximg/shock_cedd46b4f0b92ad9a20dd83b9a92109eb6216088.png" alt="shock_cedd46b4f0b92ad9a20dd83b9a92109eb6216088.png"></li>
<li>विद्युत ऊर्जा = (कालावधी * व्होल्टेजचा वर्ग / विद्युतरोध)= कालावधी x विद्युत शक्ती<br>
<img src="ltximg/shock_a55a43bdfa1eb583eab91b60f633c76d9badfc8b.png" alt="shock_a55a43bdfa1eb583eab91b60f633c76d9badfc8b.png"></li>
</ul>
</div>
</div>
</div>
<div id="outline-container-orgfde9c69" class="outline-3">
<h3 id="orgfde9c69">थोडा अधिक तपशील</h3>
<div class="outline-text-3" id="text-orgfde9c69">
<ul class="org-ul">
<li>शरिराचा कोणताही भाग विद्युत मंडलाचा (electric circuit) चा घटक बनल्यास त्यातून प्रवाह जातो व धक्का बसतो.</li>
<li>सर्वसाधारण माणसाला बसणारा विजेचा धक्का ८० पेक्षा जास्त व्होल्टेज असेल तर जाणवतो.</li>
<li>घरगुती विजेच्या व्होल्टेज (२३० व्होल्ट) चा धक्का जोरदार असतो पण प्राणघातक असण्याची शक्यता कमी असते.</li>
<li>कारखान्यातील व्होल्टेज (४१५ व्होल्ट) चा धक्का प्राणघातक ठरू शकतो.</li>
<li>पायात रबरी, प्लास्टिकची पादत्राणे घातल्यास विजेच्या धक्क्यापासून रक्षण होऊ शकते मात्र त्यावेळी शरीराचे इतर दोन भाग त्यावेळी सर्किट मधे 
असता कामा नयेत.</li>
</ul>
</div>
</div>
<div id="outline-container-org900a831" class="outline-3">
<h3 id="org900a831">विजेच्या धक्क्याचे दुष्परिणाम</h3>
<div class="outline-text-3" id="text-org900a831">
<ul class="org-ul">
<li>ज्या ठिकाणी विजेचा स्पर्श शरिराला होतो तेथे भाजते.</li>
<li>स्नायूंची नैसर्गिक हालचाल त्यातील पोटॅशियम व सोडियम आयनांच्या प्रवाहामुळे होत असते. विद्युत ऊर्जा शरिरात बाहेरून शिरली तर आयनांच्या 
प्रवाहात उलथापालथ होते. त्यामुळे अचानक व वेडीवाकडी हालचाल होते.</li>
<li>ह्रदयासारख्या स्नायुमय अवयवांच्या कार्यात मोठा बिघाड होऊ शकतो.</li>
<li>मोठ्या प्रमाणात विद्युतऊर्जा शरिरात शिरली तर मृत्यू ओढवतो.</li>
</ul>
</div>
</div>
<div id="outline-container-orge03d49e" class="outline-3">
<h3 id="orge03d49e">विजेच्या धक्क्याबद्दलचे गैरसमज</h3>
<div class="outline-text-3" id="text-orge03d49e">
<ul class="org-ul">
<li>एसी विजेचा धक्का दूर फेकतो, डीसी विजेचा धक्का ओढून घेतो. (असे काहीही होत नाही. केवळ अचानक व वेडीवाकडी हालचाल होते.)</li>
<li>पायात चप्पल असेल तर धक्का बसत नाही. (दोन हातात दोन व्होल्टेज बिंदू धरले तर चपलेचा काय उपयोग ?)</li>
<li>स्वतःच्या हातात लाकूड घेऊन विजेचा धक्का बसत असलेल्या व्यक्तीला वाचवता येते. (या व्यक्तीला स्पर्श न करता, लाकूड किंवा कोणत्याही इतर
विद्युतरोधकाचा वापर करून सर्किटमधून बाहेर ढकलणे किंवा खेचणे आवश्यक आहे. आपण लाकूड डाव्या हातात धरून उजव्या हाताने अशा व्यक्तीला
वाचवण्यासाठी स्पर्श करू नये.)</li>
</ul>
</div>
</div>
<div id="outline-container-org126e53b" class="outline-3">
<h3 id="org126e53b">विजेच्या धक्क्याचे उपयोग</h3>
<div class="outline-text-3" id="text-org126e53b">
<ul class="org-ul">
<li>अगदी कमी व्होल्टेज व कमी ऊर्जेचा उपयोग करून शरिरातील काही व्याधी घालवता येतात. फिजिओथेरपी या वैद्यकीय शाखेत याचा अधिक अभ्यास
केला गेला आहे. मात्र मानसिक रोगांच्या बाबतीत असा उपयोग हल्ली केला जात नाही.</li>
</ul>

<hr>
<p>
तुमच्या निवडक प्रतिक्रिया या लेखाच्या खाली प्रसिद्ध केल्या जातील. तुमची प्रतिक्रिया तुम्ही इमेल ने पुढील पत्त्यावर पाठवू शकता.
<img src="../img/vidnyanmail.png" alt="vidnyanmail.png">
</p>
<hr>
</div>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="author">Author: विज्ञानदूत</p>
<p class="date">Created: 2021-09-26 Sun 16:55</p>
<p class="validation"></p>
</div>
</body>
</html>
