<!DOCTYPE html>
<html lang="mr">
<head>
<!-- 2021-08-22 Sun 09:12 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>विज्ञान केंद्र - लेखमाला</title>
<meta name="generator" content="Org mode">
<meta name="author" content="विज्ञानदूत">
<meta name="keywords" content="चक्रवाढ व्याज, लिंबू, साधे वैज्ञानिक प्रयोग, घरबाग, विज्ञान कथा">
<style type="text/css">
 <!--/*--><![CDATA[/*><!--*/
  .title  { text-align: center;
             margin-bottom: .2em; }
  .subtitle { text-align: center;
              font-size: medium;
              font-weight: bold;
              margin-top:0; }
  .todo   { font-family: monospace; color: red; }
  .done   { font-family: monospace; color: green; }
  .priority { font-family: monospace; color: orange; }
  .tag    { background-color: #eee; font-family: monospace;
            padding: 2px; font-size: 80%; font-weight: normal; }
  .timestamp { color: #bebebe; }
  .timestamp-kwd { color: #5f9ea0; }
  .org-right  { margin-left: auto; margin-right: 0px;  text-align: right; }
  .org-left   { margin-left: 0px;  margin-right: auto; text-align: left; }
  .org-center { margin-left: auto; margin-right: auto; text-align: center; }
  .underline { text-decoration: underline; }
  #postamble p, #preamble p { font-size: 90%; margin: .2em; }
  p.verse { margin-left: 3%; }
  pre {
    border: 1px solid #ccc;
    box-shadow: 3px 3px 3px #eee;
    padding: 8pt;
    font-family: monospace;
    overflow: auto;
    margin: 1.2em;
  }
  pre.src {
    position: relative;
    overflow: visible;
    padding-top: 1.2em;
  }
  pre.src:before {
    display: none;
    position: absolute;
    background-color: white;
    top: -10px;
    right: 10px;
    padding: 3px;
    border: 1px solid black;
  }
  pre.src:hover:before { display: inline;}
  /* Languages per Org manual */
  pre.src-asymptote:before { content: 'Asymptote'; }
  pre.src-awk:before { content: 'Awk'; }
  pre.src-C:before { content: 'C'; }
  /* pre.src-C++ doesn't work in CSS */
  pre.src-clojure:before { content: 'Clojure'; }
  pre.src-css:before { content: 'CSS'; }
  pre.src-D:before { content: 'D'; }
  pre.src-ditaa:before { content: 'ditaa'; }
  pre.src-dot:before { content: 'Graphviz'; }
  pre.src-calc:before { content: 'Emacs Calc'; }
  pre.src-emacs-lisp:before { content: 'Emacs Lisp'; }
  pre.src-fortran:before { content: 'Fortran'; }
  pre.src-gnuplot:before { content: 'gnuplot'; }
  pre.src-haskell:before { content: 'Haskell'; }
  pre.src-hledger:before { content: 'hledger'; }
  pre.src-java:before { content: 'Java'; }
  pre.src-js:before { content: 'Javascript'; }
  pre.src-latex:before { content: 'LaTeX'; }
  pre.src-ledger:before { content: 'Ledger'; }
  pre.src-lisp:before { content: 'Lisp'; }
  pre.src-lilypond:before { content: 'Lilypond'; }
  pre.src-lua:before { content: 'Lua'; }
  pre.src-matlab:before { content: 'MATLAB'; }
  pre.src-mscgen:before { content: 'Mscgen'; }
  pre.src-ocaml:before { content: 'Objective Caml'; }
  pre.src-octave:before { content: 'Octave'; }
  pre.src-org:before { content: 'Org mode'; }
  pre.src-oz:before { content: 'OZ'; }
  pre.src-plantuml:before { content: 'Plantuml'; }
  pre.src-processing:before { content: 'Processing.js'; }
  pre.src-python:before { content: 'Python'; }
  pre.src-R:before { content: 'R'; }
  pre.src-ruby:before { content: 'Ruby'; }
  pre.src-sass:before { content: 'Sass'; }
  pre.src-scheme:before { content: 'Scheme'; }
  pre.src-screen:before { content: 'Gnu Screen'; }
  pre.src-sed:before { content: 'Sed'; }
  pre.src-sh:before { content: 'shell'; }
  pre.src-sql:before { content: 'SQL'; }
  pre.src-sqlite:before { content: 'SQLite'; }
  /* additional languages in org.el's org-babel-load-languages alist */
  pre.src-forth:before { content: 'Forth'; }
  pre.src-io:before { content: 'IO'; }
  pre.src-J:before { content: 'J'; }
  pre.src-makefile:before { content: 'Makefile'; }
  pre.src-maxima:before { content: 'Maxima'; }
  pre.src-perl:before { content: 'Perl'; }
  pre.src-picolisp:before { content: 'Pico Lisp'; }
  pre.src-scala:before { content: 'Scala'; }
  pre.src-shell:before { content: 'Shell Script'; }
  pre.src-ebnf2ps:before { content: 'ebfn2ps'; }
  /* additional language identifiers per "defun org-babel-execute"
       in ob-*.el */
  pre.src-cpp:before  { content: 'C++'; }
  pre.src-abc:before  { content: 'ABC'; }
  pre.src-coq:before  { content: 'Coq'; }
  pre.src-groovy:before  { content: 'Groovy'; }
  /* additional language identifiers from org-babel-shell-names in
     ob-shell.el: ob-shell is the only babel language using a lambda to put
     the execution function name together. */
  pre.src-bash:before  { content: 'bash'; }
  pre.src-csh:before  { content: 'csh'; }
  pre.src-ash:before  { content: 'ash'; }
  pre.src-dash:before  { content: 'dash'; }
  pre.src-ksh:before  { content: 'ksh'; }
  pre.src-mksh:before  { content: 'mksh'; }
  pre.src-posh:before  { content: 'posh'; }
  /* Additional Emacs modes also supported by the LaTeX listings package */
  pre.src-ada:before { content: 'Ada'; }
  pre.src-asm:before { content: 'Assembler'; }
  pre.src-caml:before { content: 'Caml'; }
  pre.src-delphi:before { content: 'Delphi'; }
  pre.src-html:before { content: 'HTML'; }
  pre.src-idl:before { content: 'IDL'; }
  pre.src-mercury:before { content: 'Mercury'; }
  pre.src-metapost:before { content: 'MetaPost'; }
  pre.src-modula-2:before { content: 'Modula-2'; }
  pre.src-pascal:before { content: 'Pascal'; }
  pre.src-ps:before { content: 'PostScript'; }
  pre.src-prolog:before { content: 'Prolog'; }
  pre.src-simula:before { content: 'Simula'; }
  pre.src-tcl:before { content: 'tcl'; }
  pre.src-tex:before { content: 'TeX'; }
  pre.src-plain-tex:before { content: 'Plain TeX'; }
  pre.src-verilog:before { content: 'Verilog'; }
  pre.src-vhdl:before { content: 'VHDL'; }
  pre.src-xml:before { content: 'XML'; }
  pre.src-nxml:before { content: 'XML'; }
  /* add a generic configuration mode; LaTeX export needs an additional
     (add-to-list 'org-latex-listings-langs '(conf " ")) in .emacs */
  pre.src-conf:before { content: 'Configuration File'; }

  table { border-collapse:collapse; }
  caption.t-above { caption-side: top; }
  caption.t-bottom { caption-side: bottom; }
  td, th { vertical-align:top;  }
  th.org-right  { text-align: center;  }
  th.org-left   { text-align: center;   }
  th.org-center { text-align: center; }
  td.org-right  { text-align: right;  }
  td.org-left   { text-align: left;   }
  td.org-center { text-align: center; }
  dt { font-weight: bold; }
  .footpara { display: inline; }
  .footdef  { margin-bottom: 1em; }
  .figure { padding: 1em; }
  .figure p { text-align: center; }
  .inlinetask {
    padding: 10px;
    border: 2px solid gray;
    margin: 10px;
    background: #ffffcc;
  }
  #org-div-home-and-up
   { text-align: right; font-size: 70%; white-space: nowrap; }
  textarea { overflow-x: auto; }
  .linenr { font-size: smaller }
  .code-highlighted { background-color: #ffff00; }
  .org-info-js_info-navigation { border-style: none; }
  #org-info-js_console-label
    { font-size: 10px; font-weight: bold; white-space: nowrap; }
  .org-info-js_search-highlight
    { background-color: #ffff00; color: #000000; font-weight: bold; }
  .org-svg { width: 90%; }
  /*]]>*/-->
</style>
<link rel="stylesheet" type="text/css" href="babastyle.css" />
<script type="text/javascript">
/*
@licstart  The following is the entire license notice for the
JavaScript code in this tag.

Copyright (C) 2012-2018 Free Software Foundation, Inc.

The JavaScript code in this tag is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this tag.
*/
<!--/*--><![CDATA[/*><!--*/
 function CodeHighlightOn(elem, id)
 {
   var target = document.getElementById(id);
   if(null != target) {
     elem.cacheClassElem = elem.className;
     elem.cacheClassTarget = target.className;
     target.className = "code-highlighted";
     elem.className   = "code-highlighted";
   }
 }
 function CodeHighlightOff(elem, id)
 {
   var target = document.getElementById(id);
   if(elem.cacheClassElem)
     elem.className = elem.cacheClassElem;
   if(elem.cacheClassTarget)
     target.className = elem.cacheClassTarget;
 }
/*]]>*///-->
</script>
</head>
<body>
<div id="content">
<header>
<h1 class="title">विज्ञान केंद्र - लेखमाला</h1>
</header><hr>
<p>
<a href="../index.html"><b>मुख्यपान</b></a>&emsp;&emsp;&emsp;&emsp;
<a href="../freeprojects.html"><b>मुक्त प्रकल्प</b></a>&emsp;&emsp;&emsp;&emsp;
<a href="../articles.html"><b>लेखमाला</b></a>&emsp;&emsp;&emsp;&emsp;
<a href="../activities.html"><b>उपक्रम</b></a>&emsp;&emsp;&emsp;&emsp;  
<a href="../contact.html"><b>संपर्क</b></a>&emsp;&emsp;&emsp;&emsp;  
</p>
<hr>
<div id="outline-container-org297139e" class="outline-2">
<h2 id="org297139e">विज्ञानदूत एप्रिल २०२१</h2>
<div class="outline-text-2" id="text-org297139e">
<p>
<b><i>विज्ञानदूत</i></b> या विज्ञान केंद्राच्या अनियतकालिकाचा या वर्षातला हा तिसरा अंक. या अंकाचे मानकरी आहेत श्री. उदय ओक, डॉ.जयंत गाडगीळ
आणि विज्ञानदूत.
</p>
</div>
</div>
<div id="outline-container-org5975658" class="outline-2">
<h2 id="org5975658">पैशाची पिल्ले -चक्रवाढ व्याज</h2>
<div class="outline-text-2" id="text-org5975658">

<figure>
<img src="../img/moneyTree.jpg" alt="moneyTree.jpg">

<figcaption><span class="figure-number">Figure 1: </span>पैशाचे झाड ?</figcaption>
</figure>

<p>
चक्रवाढ व्याजाची ओळख शाळेच्या अभ्यासात करून दिली जाते. हरित अर्थशास्त्राच्या (green economics) जगात चक्रवाढ व्याज हा एक खलनायकच आहे. 
एखाद्या गोष्टीसाठी भांडवल पुरवण्याबद्दल धनकोला ऋणकोने द्यायचे मूल्य म्हणजे व्याज. कर्ज घेणारा मूळ रक्कम परत देताना हे मूल्यही देतो. 
ठराविक काळानंतर मूळ मुद्दल परत दिले नाही तर त्यावरचे व्याज मूळ मुद्दलात शिरते आणि मुद्दल फुगते. नव्या मुद्दलावर ठरलेल्या दराने व्याज द्यावे लागते.
कर्ज परत करेपर्यंत हे चक्र वाढतच जाते. त्यामुळे व्याज व मुद्दलही वाढते. या पद्धतीला चक्रवाढ व्याज (compound interest) म्हणतात.
</p>
</div>

<div id="outline-container-orgda67f28" class="outline-3">
<h3 id="orgda67f28">चक्रीय व्याज पद्धती मागील गणित</h3>
<div class="outline-text-3" id="text-orgda67f28">

<figure>
<img src="ltximg/doot0421_ef361b955a4ac5211be89620abd1a63ef50f81b1.png" alt="doot0421_ef361b955a4ac5211be89620abd1a63ef50f81b1.png">
</figure>

<p>
वरील समीकरणात,
</p>
<ul class="org-ul">
<li><img src="ltximg/doot0421_533e18341d46eb6670a308c4f8804b53ff018705.png" alt="doot0421_533e18341d46eb6670a308c4f8804b53ff018705.png"> म्हणजे मुद्दल+व्याज म्हणजे रास (principal+interest)</li>
<li><img src="ltximg/doot0421_e308d243675bf81ea9d9e5952cea50b677b152eb.png" alt="doot0421_e308d243675bf81ea9d9e5952cea50b677b152eb.png"> म्हणजे मूळ मुद्दल (principal)</li>
<li><img src="ltximg/doot0421_4e9516b71ebcc533dd3918483d513ab6ba214200.png" alt="doot0421_4e9516b71ebcc533dd3918483d513ab6ba214200.png"> म्हणजे व्याजाचा दर (rate of interest)</li>
<li><img src="ltximg/doot0421_3773bfa0a05421f0f67e6c4791a370dbe60310e2.png" alt="doot0421_3773bfa0a05421f0f67e6c4791a370dbe60310e2.png"> म्हणजे वर्षांमधे व्यक्त केलेली मुदत (tenure)</li>
</ul>
<p>
व्याजाचा दर (r) हा नेहमीच <i>दर साल दर शेकडा r टक्के</i> असा सांगितला जातो. वरील सूत्र व्यवहारात वापरले जाते तेव्हा त्या सूत्रात
१ रुपयावरील व्याज r या वार्षिक दराने (r हा अपूर्णांक असणे अपेक्षित आहे. उदा. 0.06 म्हणजे ६ टक्के) विचारात घेतले जाते. 
व्याज मुद्दलात किती काळाने सामील होईल आणि त्यामुळे कर्ज परत करेपर्यंत अशी किती आवर्तने होतील हे महत्वाचे असते.
</p>
<ul class="org-ul">
<li>व्याज हे प्रत्येक वर्षानंतर मुद्दलात सामील होणार असेल तर दोन वर्षांसाठी दिलेल्या कर्जात व्याज सामील होण्याची दोन आवर्तने होतील. येथे वरील
सूत्र थेट वापरता येते.</li>
<li><p>
व्याज हे प्रत्येक सहा महिन्यांनंतर मुद्दलात सामील होणार असेल तर दोन वर्षांसाठी दिलेल्या कर्जात व्याज सामिल होण्याची चार आवर्तने होतील. आणि पुढील सूत्र वापरले जाईलः
</p>

<figure>
<img src="ltximg/doot0421_0e30c1e2fdacbe3b4f75c410611a4cf19666a2dd.png" alt="doot0421_0e30c1e2fdacbe3b4f75c410611a4cf19666a2dd.png">
</figure></li>

<li><p>
व्याज हे प्रत्येक तीन महिन्यांनंतर मुद्दलात सामील होणार असेल तर दोन वर्षांसाठी दिलेल्या कर्जात व्याज सामील होण्याची आठ आवर्तने होतील. 
</p>

<figure>
<img src="ltximg/doot0421_26f3ec7f27dd1fa0a6b317a7464b12ef97f01fac.png" alt="doot0421_26f3ec7f27dd1fa0a6b317a7464b12ef97f01fac.png">
</figure></li>
<li><p>
वरील सूत्रे नीट समजून घेतली तर <b><i>दर दिवशी मुद्दलात सामील होणारे</i></b> व्याज+मुद्दल असे काढता येतेः
</p>

<figure>
<img src="ltximg/doot0421_1f21915ea03ea9a9cfa826b8fafacbb2d0ecea0c.png" alt="doot0421_1f21915ea03ea9a9cfa826b8fafacbb2d0ecea0c.png">
</figure>
<p>
दरसाल केवळ एक टक्का दराने १०० रुपये एक वर्षासाठी घेतले तर (दर दिवशी होणाऱ्या आवर्तनामुळे) वर्ष संपताना २७१.४६ रु. द्यावे लागतील 
असे वरील सूत्र सांगते. म्हणजे धनकोने त्याची रक्कम एकाच वर्षात २.७१ पट केली. व्यवहारात अडल्या-नडलेल्या गरजूंना अशा पद्धतीने दररोज
आवर्तने घेणारे कर्ज दिले जाते हे तुम्हाला माहिती आहे का ?
</p></li>
</ul>
</div>
</div>
<div id="outline-container-org6a886be" class="outline-3">
<h3 id="org6a886be">वेगळाच काल्पनिक गणिती प्रयोग</h3>
<div class="outline-text-3" id="text-org6a886be">
<p>
आता एक वेगळाच गणिती प्रयोग करून पाहूया. व्याज <i>दर तासाला</i> (दर दिवशी किंवा तिमाही नव्हे) आवर्तित होते असे समजून एक टक्का वार्षिक दराने घेतलेले १०० रुपये
एक वर्षानंतर फेडले तर किती रक्कम होते&#x2026;.
</p>

<figure>
<img src="ltximg/doot0421_589f0959b7e1550cdfd4aff13b4af77430977c2b.png" alt="doot0421_589f0959b7e1550cdfd4aff13b4af77430977c2b.png">
</figure>

<p>
वरील गणित सोडवले तर,
<img src="ltximg/doot0421_9971628bbdaa3fa649a807795f3ca51f1b69690a.png" alt="doot0421_9971628bbdaa3fa649a807795f3ca51f1b69690a.png">
म्हणजे २७१.८१ रुपये इतकी रक्कम परत द्यावी लागते. दर दिवशी होणारे आवर्तन आणि दर तासाला होणारे आवर्तन यामुळे परत करण्याच्या रकमेत फक्त
३५ पैशाचा फरक पडतो. याचे कारण कलनशास्त्र (calculus) सांगू शकते:
</p>

<p>
जेव्हा n ही फार मोठी संख्या असते व वाढतच जाते, त्यावेळी <img src="ltximg/doot0421_d5519e7a8a868bb894e7a6cf47cf26b085f1a649.png" alt="doot0421_d5519e7a8a868bb894e7a6cf47cf26b085f1a649.png"> या संख्येची किंमत 2.7182818 या संख्येच्या जवळ जात (converge) रहाते.
या संख्येला गणितात <img src="ltximg/doot0421_d02ed2782cd50a56baba2e3580944e581dac63fc.png" alt="doot0421_d02ed2782cd50a56baba2e3580944e581dac63fc.png"> ही खास संज्ञा वापरली जाते. नेपियर या गणितज्ञाच्या सन्मानार्थ तिला नेपेरियन बेस असे म्हणतात. या संख्येमुळे गणितातील अनेक
गूढे उकलता आली आहेत. दर तासाला आवर्तने घेणाऱ्या व्याजामुळे 271.81 रु. द्यावे लागतात. या संख्येत आणि <i>e</i> या संख्येत तुम्हाला काही 
साम्य आढळते का ?
</p>
</div>
</div>
</div>
<div id="outline-container-orgbf8cc15" class="outline-2">
<h2 id="orgbf8cc15">थर पद्धत</h2>
<div class="outline-text-2" id="text-orgbf8cc15">
<p>
<a href="./bhajibaug.html">घर तेथे भाजीबाग</a> या उपक्रमाच्या अंतर्गत विज्ञान केंद्रातर्फे सर्वांना आपल्या घरात ताजी भाजी लावायला प्रोत्साहन दिले जाते. नवी भाजी लावताना पुढील
प्रकारे लावली तर भाजी उत्तम तर येईलच पण स्वयंपाकघरातल्या ओल्या कचऱ्याचा प्रश्नही मिटेल.
</p>


<figure>
<img src="../img/tharKundi.jpg" alt="tharKundi.jpg">

<figcaption><span class="figure-number">Figure 2: </span>जैविक कचऱ्याच्या थरांचे कुंड</figcaption>
</figure>

<p>
वरील आकृतीत जैविक कचऱ्याच्या ४ थरांची कुंड दाखवले आहे. या कुंडाच्या भिंती विटा रचून बनवल्या आहेत. एकेका भिंतीत ६ विटा वापरल्या आहेत.
अशा चार भिंतींमधे १८ इंच x १८ इंच x ९ इंच अशी जागा तयार होते (घनफळ १.६८ घनफूट). या जागेत जैविक कचऱ्याचे
चार थर टाकून कुंड भरले तर त्यात लावलेली भाजी उत्तम येते. 
</p>
<ol class="org-ol">
<li>सर्वात खालचा थर (१) वाळलेल्या काड्या-काटक्या, नारळाच्या करवंट्या, शहाळ्याचे तुकडे अशा कडक, वाळक्या कचऱ्याचा असेल.</li>
<li>क्र. २ चा थर वाळलेली पाने, नारळाच्या शेंड्या, वाळलेले गवत अशा कचऱ्याचा असेल.</li>
<li>क्र. ३ चा थर हिरवा पाला (उदा. सुबाभूळ, घरातील भाजीच्या काड्या) फळांची ओली साले इत्यादी कचऱ्याचा असेल.</li>
<li>क्र. ४ चा थर माती, जैविक खत (उदा. गांडूळ खत, वाळलेले शेणखत) यांच्या मिश्रणाचा असेल.</li>
<li>ही जागा भरून झाली की त्यावर रवंथ करणाऱ्या प्राण्याचे शेण व मूत्र (सुमारे १०० ग्रॅम) व एक लिटर पाणी यांचे मिश्रण शिंपडावे.</li>
</ol>

<p>
वरील प्रमाणे कुंड तयार केले तर या कुंडात लावलेली भाजी उत्तम पोसली जाईल. सर्वात वरच्या थरात लगेचच बी पेरण्यास हरकत नाही. जसे
हे बी रुजून वर येईल, त्याला खालील जैविक कचऱ्यातून विविध पोषकद्रव्ये मिळत जातील. रोप मोठे होताना खालच्या बाजूला जैविक कचरा कुजत
जातो आणि रोपाच्या वाढीला मदत करतो. ही कुंडी तुम्ही कोठेही तयार करू शकता. त्यासाठी जमीन खणावी लागत नाही. नर्सरीत वापरल्या जाणाऱ्या
मोठ्या काळ्या पिशव्यांमधेही असे चार थर तयार करून त्यात भाजी लावता येईल. सर्वात खालच्या थरातील लाकूड व नारळाचे कवच अतिशय सावकाश कुजते.
पण रोपाच्या मुळांना त्यामुळे खालच्या बाजूने हवा मिळत राहते. सर्व जागा जैविक कचऱ्याने भरलेली असल्यामुळे पाणी शोषून धरले जाते. झाडाच्या
मुळाचे तापमान स्थिर (२३ ते २७ अंश सेल्सियस) ठेवण्यासही त्यामुळे मदत होते. 
</p>

<p>
जसजसा जैविक कचरा कुजत जातो तसे त्याचे आकारमान कमी होते आणि झाडाच्या मुळाशी नवा, ओला जैविक कचरा टाकावा लागतो. त्यामुळे 
तुमच्या स्वयंपाकघरातला ओला कचऱ्याची समस्या आपोआपच नष्ट होते. या प्रकारे कुंडी तुम्ही तयार केली, तर आम्हाला जरूर कळवा.  
</p>
</div>
</div>

<div id="outline-container-orgb041588" class="outline-2">
<h2 id="orgb041588">आत्मविश्वास !</h2>
<div class="outline-text-2" id="text-orgb041588">

<figure>
<img src="../img/cvRaman.jpg" alt="cvRaman.jpg">

<figcaption><span class="figure-number">Figure 3: </span>डॉ. चंद्रशेखर वेंकट रमण</figcaption>
</figure>

<p>
थोर भारतीय शास्त्रज्ञ चंद्रशेखर वेंकट रमण यांना आपल्याला भौतिकशास्त्रातील नोबेल पारितोषिक मिळेलच याचा ठाम विश्वास होता. 
</p>

<p>
१९२८ साली (त्या वर्षी ओवेन रिचर्डसन यांना नोबेल मिळाले) आणि १९२९ साली (त्या वर्षी लुई दि ब्रॉग्ली यांना नोबेल मिळाले) त्यांची निराशा झाली. 
पण १९३० साली त्यांचा विश्वास एवढा दृढ होता की पारितोषिक समारंभाला जाण्यासाठी त्यांनी जुलैमध्येच तिकीट काढून ठेवले. 
पारितोषिक नोव्हेंबरमध्ये जाहीर होते आणि पारितोषिक प्रदान सोहळा डिसेंबरमध्ये होतो. त्यांच्या अपेक्षेप्रमाणे त्यांना १९३० चे भौतिकशास्त्रातले नोबेल मिळाले. हे पारितोषिक (भौतिकशास्त्रातील नोबेल) मिळवणारे ते पहिले आशियाई आणि पहिले गौरेतर होते.
</p>

<p>
त्याआधी १९१३ साली रवींद्रनाथ टागोर यांना साहित्यातील नोबेल पारितोषिक मिळाले होते.
चंद्रशेखर वेंकट रमण यांचा जन्म ७ नोव्हेंबर १८८८ साली तामिळनाडूमध्ये झाला. त्यांनी लावलेला शोध 'रमण परिणाम' म्हणून ओळखला जातो.
</p>
</div>
</div>
<div id="outline-container-org62f7293" class="outline-2">
<h2 id="org62f7293">सरबतांचे बदलते रंग&#x2026;</h2>
<div class="outline-text-2" id="text-org62f7293">
<p>
उन्हाळा आला, की परीक्षेसाठी अभ्यास करताना मला मधेच एखादे सरबत पिण्याची चैन करता येत असे. कैरीचे पिवळे पन्हे, संत्र्यांच्या हंगामात 
केलेले संत्र्याचे टिकाऊ सरबत, कधी बाहेर विकत मिळणारे खस किंवा वाळ्याचे हिरवे सरबत अशा वेगवगळ्या रंगांची सरबते पिताना माझ्या लक्षात
आले की लाल, जांभळ्या अशा अनेक रंगांची सरबते असतात. पण निळ्या रंगाचे सरबत सहसा दिसत नाही.
</p>

<p>
आमच्याकडे निळ्या गोकर्णीचा वेल होता. त्याला नुकतीच फुले आलेली होती. गोकर्णीचे फूल विषारी नसते असे माझ्या आजीने मला सांगितले होते.
म्हणून विचार केला की त्याचेच सरबत करून बघावे. तर गोकर्णीची फुले कुस्करून त्यात पाणी घालून बघितले. फार गडद नाही, पण निळसर 
रंग तर आला होता. आता सरबत म्हटल्यावर साखर मीठ हवेच. ते घातले. चव ठीक होती.
</p>

<p>
मग काय सुचले कोण जाणे, त्यात थोडे लिंबूही पिळले. आणि काय गंमत सांगू तुम्हाला? त्याचा निळा रंग गेला की. 
हा सगळा आम्लधर्मी लिंबाचा परिणाम असणार असे मी ओळखले. पण अल्कधर्मी करण्यासाठी सरबतात साबण टाकून तो वाया घालवायला मी 
काय बावळट होतो का काय? मग मी हाच प्रयोग मीठ किंवा साखर वाया न घालवता केला आणि आम्लधर्मी लिंबू लावून रंग बदलला. आणि 
थोडेसे साबणाचे बोट लावले की परत निळा रंग झाला. असाच प्रयोग मग मी दूध न घातलेला चहा घेऊन लिंबू पिळून पाहिले. चहाचा रंग 
फिकट पिवळसर झाला होता. उन्हाळ्यात उन्हात भटकल्यामुळे जो त्रास होतो, त्याकरीता मला आई कधीकधी पळसाची फुले पाण्यात घालून तयार 
होणारे केशरी पाणी प्यायला द्यायची. त्यात लिंबू पिळल्यावर त्याचाही रंग पिवळा झाला. त्यांचे रंग साबणाचे पाणी लावल्यावर पहिल्यासारखे झाले. 
हा खेळ मी पुन्हा पुन्हा करून पाहिला.
</p>

<p>
माझा मित्र विनीत मला म्हणाला, अरे हा तर विज्ञानातला एक प्रयोगच झाला.पण गोकर्णीच्या निळ्या रंगाचा लिंबू घातल्यावर कोणता रंग होतो, 
ते नाही सांगितलेस. मी म्हणालो, मी असे अनेक प्रयोग केल्यामुळे आणि ते प्रयोगवहीसारखे लिहून न ठेवल्यामुळे आता आठवत नाही. 
प्रयोगवही लिहिण्याचे महत्त्व मला जाणवले. मग विनीतला मी पुन्हा प्रयोग करायला लावला. आणि प्रयोगवहीसारखा शीर्षक, प्रयोगाचा उद्देश, 
प्रयोगासाठी लागणारी साधने किंवा उपकरणे, रसायने, तसेच संगतवार कृती, निरीक्षणे, अनुमान आणि निष्कर्ष असे सगळे व्यवस्थित लिहायला 
सांगितले. तर त्याने सांगितले की निळ्या गोकर्णीचा रंग फिका जांभळा होतो.
</p>

<p>
मग मला प्रश्न पडला की असे कोणकोणत्या फुलांचे रंग आम्लधर्मी किंवा अल्कधर्मी रसायनांमुळे बदलतात? कोणत्याही चुणचुणीत विद्यार्थी किंवा विद्यार्थिनीला हे प्रयोग करुन बघता येतील आणि 
माझ्या प्रश्नाचे उत्तरही शोधता येईल. शिवाय तुम्हाला अजून काही प्रश्न पडले तर तेही विचारता येतील.
</p>
</div>
</div>
<div id="outline-container-orga7cc466" class="outline-2">
<h2 id="orga7cc466">कोडे</h2>
<div class="outline-text-2" id="text-orga7cc466">
</div>
<div id="outline-container-org90e7710" class="outline-3">
<h3 id="org90e7710">मागील कोड्याचे उत्तर</h3>
<div class="outline-text-3" id="text-org90e7710">
<p>
<a href="./doot0321.html">सलिलने किती वेळ अभ्यास केला?</a> या प्रश्नाचे उत्तर असे आहेः 
</p>
<ul class="org-ul">
<li>सलिलने <img src="ltximg/doot0421_3773bfa0a05421f0f67e6c4791a370dbe60310e2.png" alt="doot0421_3773bfa0a05421f0f67e6c4791a370dbe60310e2.png"> तास अभ्यास केला असे मानू.</li>
<li>जितका काळ अभ्यास झाला त्या प्रमाणात प्रत्येक मेणबत्तीची लांबी कमी झाली.</li>
<li>या काळा नंतर मोठ्या मेणबत्तीचे <img src="ltximg/doot0421_7c07242d5012c1c66525704ecc5c41628f9bac83.png" alt="doot0421_7c07242d5012c1c66525704ecc5c41628f9bac83.png"> इतके तास शिल्लक राहिले. तर लहान मेणबत्तीचे <img src="ltximg/doot0421_db17f696b446316349ae1e421c6161890ce616c0.png" alt="doot0421_db17f696b446316349ae1e421c6161890ce616c0.png"> इतके तास शिल्लक राहिले.</li>
<li><p>
जाड मेणबत्तीची उरलेली लांबी बारीक मेणबत्तीच्या उरलेल्या लांबीच्या दुप्पट होती. म्हणजेच
</p>

<figure>
<img src="ltximg/doot0421_ab76fd4fcded2f48bad9acebc6217555948101bf.png" alt="doot0421_ab76fd4fcded2f48bad9acebc6217555948101bf.png">
</figure></li>
</ul>
<p>
तिरकस गुणाकार करून वरील समीकरण सोडवले असता, <img src="ltximg/doot0421_d05a3eb95b2df5f18cf57ae491d52c6372f9d254.png" alt="doot0421_d05a3eb95b2df5f18cf57ae491d52c6372f9d254.png"> हे उत्तर मिळते. <b>म्हणजे सलिलने २ तास अभ्यास केला.</b>
</p>
</div>
</div>
<div id="outline-container-org842d946" class="outline-3">
<h3 id="org842d946">नवे कोडे</h3>
<div class="outline-text-3" id="text-org842d946">

<figure>
<img src="../img/marbles.jpg" alt="marbles.jpg">

<figcaption><span class="figure-number">Figure 4: </span>गोट्या</figcaption>
</figure>

<p>
अजय, विजय आणि सुजय हे तिघेही शाळासोबती. गोट्या खेळण्यात कोण जास्त पटाईत याबाबत काहीही बोलणे अवघड होते.
एका रविवारी तिघेही अजयच्या घरी गप्पा मारत बसले होते. त्यांच्या चड्ड्यांचे खिसे अर्थातच गोट्यांनी भरले होते. अजयला एक खेळ सुचला.
</p>

<p>
त्याने विजय आणि सुजयला त्यांच्याकडे किती गोट्या आहेत ते विचारले. त्या प्रत्येकाकडे जितक्या गोट्या होत्या, तितक्याच गोट्या अजयने त्यांना दिल्या (म्हणजे विजयकडे पाच असतील, तर त्याला पाच दिल्या; सुजयकडे सात असतील, तर त्याला सात दिल्या). मग विजयलाही त्याने तेच करायला लावले - अजयकडे असतील तेवढ्या गोट्या त्याला देणे आणि सुजयकडे असतील तेवढ्या गोट्या त्याला देणे. आणि शेवटी सुजयनेही हेच केले.
आता प्रत्येकाकडे २४ गोट्या झाल्या.
</p>

<p>
सुरुवातीला प्रत्येकाकडे किती गोट्या होत्या?
</p>
</div>
</div>
</div>
<div id="outline-container-org5f0be34" class="outline-2">
<h2 id="org5f0be34">चवदार लिंबू</h2>
<div class="outline-text-2" id="text-org5f0be34">

<figure>
<img src="../img/lemon.JPG" alt="lemon.JPG">

<figcaption><span class="figure-number">Figure 5: </span>लिंबू</figcaption>
</figure>

<p>
<a href="https://en.wikipedia.org/wiki/Lemon">चित्र: विकिपिडियाच्या सौजन्याने</a>
</p>

<p>
भारतीय जेवणात लिंबाला विशेष स्थान आहे. लिंबाचा उगमही भारतात आसाम मधे झाला असावा असा वनस्पतीशास्त्रज्ञांचा कयास आहे. रूटेसी
परिवारातील या सदाहरित झाडाचे शास्त्रीय नाव आहे सायट्रस लिमॉन (Citrus Limon).
</p>

<p>
साधारण लंबगोलाकार (ellipsoidal) व पिकल्यावर पिवळे जर्द होणारे फळ असणारे हे झाड आहे. फळाचा रस, सालाचा लगदा खाद्य
म्हणून आणि सफाईसाठी वापरला जातो. लिंबाच्या रसात ५ ते ६ टक्के सायट्रिक आम्ल असते या आम्लाचे pH मूल्य सुमारे २.२ असते यामुळेच
लिंबाचा रस आंबट लागतो. आंबटपणा सोबत रसाला एक वेगळाच ताजा स्वाद असतो त्यामुळे सरबतासाठी लिंबू वापरले जाते.  
</p>

<figure>
<img src="../img/lemonFruitLeafFlower.jpg" alt="lemonFruitLeafFlower.jpg">

<figcaption><span class="figure-number">Figure 6: </span>लिंबू-फळे,फुले,पाने</figcaption>
</figure>

<p>
<a href="https://en.wikipedia.org/wiki/Lemon">चित्र: विकिपिडियाच्या सौजन्याने</a>
</p>

<p>
लिंबात मोठ्या प्रमाणात क जीवनसत्व असते. व्यक्तीची क जीवनसत्वाची बरीचशी गरज लिंबू जेवणात वापरल्याने भरून निघते. लिंबाची लोणची 
भारतात पिढ्यान् पिढ्या केली व खाल्ली जातात. ताज्या लिंबाची फोड भारतीय जेवणात असते. ताज्या लिंबाचे सरबत उन्हाळ्यात आणि इतरही
ऋतूंत अनेकांना आवडते. त्या शिवाय लिंबाचे आणखी काही उपयोग असल्याचे सिद्ध झाले आहेः 
</p>
<ul class="org-ul">
<li>खास कीण्वन प्रक्रिया माहीत होण्याआधी, लिंबाच्या रसाचा उपयोग सायट्रिक आम्ल मिळवायला केला जात असे.</li>
<li>लिंबापासून बनवलेले तेल गंधोपचार पद्धतीत (aroma therapy) वापरले जाते. त्याचा रोगप्रतिकारक शक्ती वाढवण्यास उपयोग होत नाही पण
शरीर तणावमुक्त ठेवण्यासाठी होतो हे आता माहीत झाले आहे.</li>
<li>लिंबाच्या फळात इलेक्ट्रोड्स खोचून "बॅटरी" तयार करता येते. अशा अनेक "बॅटऱ्या" एकसर पद्धतीने (series) जोडल्यास मिळणारे व्होल्टेज
एखादे क्वार्ट्झ घड्याळ चालवण्यास पुरेसे असते. लिंबाखेरीज इतर काही फळे व वनस्पतींमधेही हा गुणधर्म आढळतो.</li>
<li>लिंबाच्या रसाने लिहिलेला मजकूर अदृश्य असतो. मात्र तसे लिहिलेला कागद गरम केल्यास हा मजकूर दिसू लागतो.</li>
<li>केसांना सोनेरी छटा आणण्यासाठी लिंबाचा रस वापरला जातो.</li>
</ul>
<p>
वर्षभर ७ अंश सेल्सियसच्या वर तापमान असणाऱ्या भूभागात लिंबाचे झाड उत्तम वाढू शकते. भरपूर फळे येण्यासाठी लिंबाच्या झाडाची छाटणी करावी लागते.
</p>
</div>
</div>

<div id="outline-container-orgac51473" class="outline-2">
<h2 id="orgac51473">हे पुस्तक जरूर वाचा</h2>
<div class="outline-text-2" id="text-orgac51473">
<p>
<i>विज्ञानावर आधारित</i> अशी जाहिरात झालेले चित्रपट पाहिले की यात थोडेतरी विज्ञान हवे अशी मागणी अनेकदा जाणकारांना करावी लागते ! 
कथांची स्थितीही काही निराळी नाही. त्यासाठी वैज्ञानिकांनीच अशी साहित्य निर्मिती केली पाहिजे. 
कार्ल सेगन, आयझॅक असिमॉव्ह या सारख्या परकीय वैज्ञानिकांनी दर्जेदार विज्ञान कथांची निर्मिती केली. तशी निर्मिती मराठीत नामवंत भारतीय गणितज्ञ व अवकाश शास्त्रज्ञ डॉ.जयंत नारळीकर
यांनी केली आहे. 
</p>


<figure>
<img src="../img/yakshDengi.jpg" alt="yakshDengi.jpg">

<figcaption><span class="figure-number">Figure 7: </span>डॉ.नारळीकरांच्या विज्ञान नवलकथा</figcaption>
</figure>


<p>
<a href="https://www.goodreads.com/book/show/16281271">यक्षांची देणगी</a> हे त्यांचे विज्ञान नवलकथांचे पुस्तक प्रसिद्ध आहे. संगणक, परग्रहवासी, कालप्रवासी, कृत्रिम बुद्धिमत्ता, अशा अनेक विषयांवर रोचक
कथा या पुस्तकात तुम्हाला वाचता येतील.  ज्या शास्त्रीय संकल्पना कथांमधे वापरल्या आहेत त्यांची परिशिष्टात थोडक्यात ओळख करून दिली आहे. 
विज्ञानाशी संबंधित नसणाऱ्या वाचकांनी हे परिशिष्ट आधी व नंतरही वाचावे. वाचकांना कथासूत्र समजायला या परिशिष्टाचा उपयोग नक्कीच होईल. 
</p>

<p>
पण वैज्ञानिक कथांनी करमणूकही केलीच पाहिजे. त्यामुळे कल्पनाशक्तीचा आविष्कारही इथे हवाच. भारतीय मातीत घडणाऱ्या या कथा असल्यामुळे
या संस्कृतीची बरी-वाईट झलकही इथे दिसली पाहिजे. कथेतले विज्ञान जड होऊ नये म्हणून थोडी विनोदाची पखरणही हवी. या साऱ्या अपेक्षा या कथा पूर्ण करतात.
</p>

<p>
<i>उजव्या सोंडेचा गणपती</i> या कथेत भारतीय संस्कृतीचा संदर्भ घेऊन मोबियस पट्टी सारख्या द्विमित अवकाशाची संकल्पना वापरली आहे. <i>गंगाधरपंतांचे पानिपत</i> या कथेतले
गंगाधरपंत नामवंत इतिहासतज्ञ आहेत पण प्रत्येक सभेला अध्यक्ष होण्याच्या त्यांच्या इच्छेची खिल्ली उडवली आहे. अनेक शक्यतांचा सिद्धांताचा (catastrophe theory) परिणाम म्हणून पानिपतच्या
लढाईचा निकाल बदलला असता का ? याचे उत्तर ही कथा देते. <i>पुत्रवती भव</i> या कथेत स्त्रिया-मुलींना कनिष्ठ दर्जा देणाऱ्या भारतीय परंपरेला
विज्ञान कथेचा वापर करून कानपिचक्याही दिल्या आहेत. अशा एकूण बारा कथा या पुस्तकात वाचता येतात.
</p>

<p>
जनसामान्यांच्या अंधश्रद्धेवर टीका करताना ती जळजळीत असू नये, त्यामुळे सामान्य माणूस वैज्ञानिक विचारांपासून पळून जातो असे मत डॉ. नारळीकरांनी 
पूर्वीच व्यक्त केले आहे. या विचाराला अनुसरून साखरेचा लेप दिलेली औषधाची गोळी द्यावी तशा या कथा आहेत. त्या करमणूक करतातच पण प्रबोधनही
करतात. त्या दृष्टीने पुस्तकाची लेखकांनी लिहिलेली प्रस्तावनाही अत्यंत वाचनीय आहे.
</p>
<hr>
<p>
हा अंक छापता येईल अशा रूपात (pdf form) तुम्हाला निःशुल्क पाठवला जाईल. मात्र त्यासाठी तुम्हाला विज्ञान केंद्राचे सदस्य व्हावे लागेल. सदस्यत्व निःशुल्क आहे. सदस्य होऊन, अंक मागवण्यासाठी पुढील पत्त्यावर इमेल कराः
तुमच्या प्रतिक्रिया देखील या पत्त्यावर पाठवता येतील.
<br>
<img src="../img/vidnyanmail.png" alt="vidnyanmail.png">
</p>


<p>
निवडक प्रतिक्रिया या लेखाच्या खाली प्रसिद्ध केल्या जातील.
</p>
<hr>
</div>
</div>
<div id="outline-container-orgd4077c9" class="outline-2">
<h2 id="orgd4077c9">प्रतिक्रिया</h2>
<div class="outline-text-2" id="text-orgd4077c9">
<p>
तुमची प्रतिक्रिया पहिली असू शकते.
</p>
<hr>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="author">Author: विज्ञानदूत</p>
<p class="date">Created: 2021-08-22 Sun 09:12</p>
<p class="validation"></p>
</div>
</body>
</html>
