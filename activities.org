#+Title: विज्ञान केंद्र - उपक्रम
#+Author: विज्ञानदूत
#+Options: toc:nil num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="babastyle.css" />
-----------
@@html:<a href="./index.html"><b>मुख्यपान</b></a>&emsp;&emsp;&emsp;&emsp;@@
@@html:<a href="./freeprojects.html"><b>मुक्त प्रकल्प</b></a>&emsp;&emsp;&emsp;&emsp;@@
@@html:<a href="./articles.html"><b>लेखमाला</b></a>&emsp;&emsp;&emsp;&emsp;@@
@@html:<a href="./activities.html"><b>उपक्रम</b></a>&emsp;&emsp;&emsp;&emsp;@@ 
@@html:<a href="./contact.html"><b>संपर्क</b></a>&emsp;&emsp;&emsp;&emsp;@@ 
-------------
* नेहमीचे उपक्रम
+ [[https://vidnyankendra.wordpress.com/][विज्ञान तंत्रज्ञान विषयक लेखन]]
+ [[./freeprojects.html][मुक्त प्रकल्प]]
+ [[./download.html][अवकरण (Download)]]
+ प्रशिक्षण
  + [[./content/tripitak.html][त्रिपिटकः]] वाचन, प्रयोग व प्रथमोपचार शिबिरे आयोजित करण्यासाठी संच व प्रशिक्षक उपलब्ध
  + [[./content/pcbtraining.html][PCB निर्मितीः]] इलेक्ट्रॉनिक सर्किट्स जोडण्यास प्रिंटेड सर्किट बोर्ड तयार करण्यासाठीचे शास्त्रशुद्ध प्रशिक्षण
+ व्याख्याने
+ कार्यशाळा
-------------------
